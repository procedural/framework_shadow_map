#include "../framework/redgpu_f.h"
#include "../framework/glfw3.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"

typedef enum ShadowMapResolution {
  SHADOW_MAP_RESOLUTION_32 = 0,
  SHADOW_MAP_RESOLUTION_24 = 1,
  SHADOW_MAP_RESOLUTION_16 = 2,
} ShadowMapResolution;

float              shadowSub         = 0.5;   // Darken, min: 0, max: 1
float              biasFactor        = 0.005; // Bias,   min: 0, max: 1
RedFBool32         hardShadows       = 1;     // Hard shadows
float              shadowSoftScatter = 300;   // Soft shadows scatter, min: 0, max: 1000

RedFHandleShader * writeMapShader    = 0;
int                fboSize           = 0;
RedFHandleFbo    * fbo               = 0;
glm::mat4          lastBiasedMatrix;

void shadowMapSetup                      (int size, ShadowMapResolution resolution);
void shadowMapSetupMaterialWithShadowMap (RedFHandleMaterial material);
void shadowMapBegin                      (RedFHandleLight light, float fustrumSize, float nearClip, float farClip);
void shadowMapEnd                        (void);
void shadowMapUpdateMaterial             (RedFHandleMaterial material);

void shadowMapSetup(int size, ShadowMapResolution resolution) {
  fboSize = size;
  
  writeMapShader = redFCreateShader(1);
  redFShaderLoad(writeMapShader[0], "writeMapShader.vert.glsl", "writeMapShader.frag.glsl");

  fbo = redFCreateFbo(1);
  RedFFboSettings fboSettings;
  fboSettings.width                        = size;
  fboSettings.height                       = size;
  fboSettings.numColorbuffers              = 0;
  fboSettings.colorFormats[0]              = 0;
  fboSettings.colorFormats[1]              = 0;
  fboSettings.colorFormats[2]              = 0;
  fboSettings.colorFormats[3]              = 0;
  fboSettings.colorFormats[4]              = 0;
  fboSettings.colorFormats[5]              = 0;
  fboSettings.colorFormats[6]              = 0;
  fboSettings.colorFormats[7]              = 0;
  fboSettings.useDepth                     = 1;
  fboSettings.useStencil                   = 0;
  fboSettings.depthStencilAsTexture        = 1;
  fboSettings.textureTarget                = 0x0DE1; // GL_TEXTURE_2D
  fboSettings.internalformat               = 0x1908; // GL_RGBA
  if (resolution == SHADOW_MAP_RESOLUTION_32) {
    fboSettings.depthStencilInternalFormat = 0x81A7; // GL_DEPTH_COMPONENT32
  } else if (resolution == SHADOW_MAP_RESOLUTION_24) {
    fboSettings.depthStencilInternalFormat = 0x81A6; // GL_DEPTH_COMPONENT24
  } else {
    fboSettings.depthStencilInternalFormat = 0x81A5; // GL_DEPTH_COMPONENT16
  }
  fboSettings.wrapModeHorizontal           = 0x2900; // GL_CLAMP
  fboSettings.wrapModeVertical             = 0x2900; // GL_CLAMP
  fboSettings.minFilter                    = 0x2600; // GL_NEAREST
  fboSettings.maxFilter                    = 0x2600; // GL_NEAREST
  fboSettings.numSamples                   = 0;
  redFFboAllocate(fbo[0], 0, 0, 0, &fboSettings);
  redFFboSetDepthTextureRGToRGBASwizzles(fbo[0], 1);
  redFFboSetDepthTextureMinMagFilter(fbo[0], 0x2600, 0x2600);// GL_NEAREST, GL_NEAREST
  redFFboSetDepthTextureWrap(fbo[0], 0x2900, 0x2900); // GL_CLAMP, GL_CLAMP
  redFFboSetDepthTextureCompareModeFunc(fbo[0], 0x884E, 0x0203); // GL_COMPARE_REF_TO_TEXTURE, GL_LEQUAL
}

void shadowMapSetupMaterialWithShadowMap(RedFHandleMaterial material) {
  const char * customUniforms =
    "uniform sampler2DShadow shadowMap;" "\n"
    "uniform mat4 biasedMvp;" "\n"
    "uniform vec4 shadowUniforms;" "\n"
  ;

  const char * postFragment =
    "vec2 poissonDisk64[64] = vec2[](" "\n"
    "  vec2(-0.613392,  0.617481)," "\n"
    "  vec2( 0.170019, -0.040254)," "\n"
    "  vec2(-0.299417,  0.791925)," "\n"
    "  vec2( 0.645680,  0.493210)," "\n"
    "  vec2(-0.651784,  0.717887)," "\n"
    "  vec2( 0.421003,  0.027070)," "\n"
    "  vec2(-0.817194, -0.271096)," "\n"
    "  vec2(-0.705374, -0.668203)," "\n"
    "  vec2( 0.977050, -0.108615)," "\n"
    "  vec2( 0.063326,  0.142369)," "\n"
    "  vec2( 0.203528,  0.214331)," "\n"
    "  vec2(-0.667531,  0.326090)," "\n"
    "  vec2(-0.098422, -0.295755)," "\n"
    "  vec2(-0.885922,  0.215369)," "\n"
    "  vec2( 0.566637,  0.605213)," "\n"
    "  vec2( 0.039766, -0.396100)," "\n"
    "  vec2( 0.751946,  0.453352)," "\n"
    "  vec2( 0.078707, -0.715323)," "\n"
    "  vec2(-0.075838, -0.529344)," "\n"
    "  vec2( 0.724479, -0.580798)," "\n"
    "  vec2( 0.222999, -0.215125)," "\n"
    "  vec2(-0.467574, -0.405438)," "\n"
    "  vec2(-0.248268, -0.814753)," "\n"
    "  vec2( 0.354411, -0.887570)," "\n"
    "  vec2( 0.175817,  0.382366)," "\n"
    "  vec2( 0.487472, -0.063082)," "\n"
    "  vec2(-0.084078,  0.898312)," "\n"
    "  vec2( 0.488876, -0.783441)," "\n"
    "  vec2( 0.470016,  0.217933)," "\n"
    "  vec2(-0.696890, -0.549791)," "\n"
    "  vec2(-0.149693,  0.605762)," "\n"
    "  vec2( 0.034211,  0.979980)," "\n"
    "  vec2( 0.503098, -0.308878)," "\n"
    "  vec2(-0.016205, -0.872921)," "\n"
    "  vec2( 0.385784, -0.393902)," "\n"
    "  vec2(-0.146886, -0.859249)," "\n"
    "  vec2( 0.643361,  0.164098)," "\n"
    "  vec2( 0.634388, -0.049471)," "\n"
    "  vec2(-0.688894,  0.007843)," "\n"
    "  vec2( 0.464034, -0.188818)," "\n"
    "  vec2(-0.440840,  0.137486)," "\n"
    "  vec2( 0.364483,  0.511704)," "\n"
    "  vec2( 0.034028,  0.325968)," "\n"
    "  vec2( 0.099094, -0.308023)," "\n"
    "  vec2( 0.693960, -0.366253)," "\n"
    "  vec2( 0.678884, -0.204688)," "\n"
    "  vec2( 0.001801,  0.780328)," "\n"
    "  vec2( 0.145177, -0.898984)," "\n"
    "  vec2( 0.062655, -0.611866)," "\n"
    "  vec2( 0.315226, -0.604297)," "\n"
    "  vec2(-0.780145,  0.486251)," "\n"
    "  vec2(-0.371868,  0.882138)," "\n"
    "  vec2( 0.200476,  0.494430)," "\n"
    "  vec2(-0.494552, -0.711051)," "\n"
    "  vec2( 0.612476,  0.705252)," "\n"
    "  vec2(-0.578845, -0.768792)," "\n"
    "  vec2(-0.772454, -0.090976)," "\n"
    "  vec2( 0.504440,  0.372295)," "\n"
    "  vec2( 0.155736,  0.065157)," "\n"
    "  vec2( 0.391522,  0.849605)," "\n"
    "  vec2(-0.620106, -0.328104)," "\n"
    "  vec2( 0.789239, -0.419965)," "\n"
    "  vec2(-0.545396,  0.538133)," "\n"
    "  vec2(-0.178564, -0.596057)" "\n"
    ");" "\n"
    "" "\n"
    "vec2 poissonDisk16[16] = vec2[](" "\n"
    "  vec2(-0.94201624, -0.39906216)," "\n"
    "  vec2( 0.94558609, -0.76890725)," "\n"
    "  vec2(-0.094184101,-0.92938870)," "\n"
    "  vec2( 0.34495938,  0.29387760)," "\n"
    "  vec2(-0.91588581,  0.45771432)," "\n"
    "  vec2(-0.81544232, -0.87912464)," "\n"
    "  vec2(-0.38277543,  0.27676845)," "\n"
    "  vec2( 0.97484398,  0.75648379)," "\n"
    "  vec2( 0.44323325, -0.97511554)," "\n"
    "  vec2( 0.53742981, -0.47373420)," "\n"
    "  vec2(-0.26496911, -0.41893023)," "\n"
    "  vec2( 0.79197514,  0.19090188)," "\n"
    "  vec2(-0.24188840,  0.99706507)," "\n"
    "  vec2(-0.81409955,  0.91437590)," "\n"
    "  vec2( 0.19984126,  0.78641367)," "\n"
    "  vec2( 0.14383161, -0.14100790)" "\n"
    ");" "\n"
    "" "\n"
    "vec2 poissonDisk4[4] = vec2[](" "\n"
    "  vec2(-0.94201624, -0.39906216)," "\n"
    "  vec2( 0.94558609, -0.76890725)," "\n"
    "  vec2(-0.094184101,-0.92938870)," "\n"
    "  vec2( 0.34495938,  0.29387760)" "\n"
    ");" "\n"
    "" "\n"
    "float random(vec4 seed4) {" "\n"
    "  float dot_product = dot(seed4, vec4(12.9898, 78.233, 45.164, 94.673));" "\n"
    "  return fract(sin(dot_product) * 43758.5453);" "\n"
    "}" "\n"
    "" "\n"
    "vec4 postFragment(vec4 localColor) {" "\n"
    "  float shadowSub = shadowUniforms.x;" "\n"
    "  float hardShadows = shadowUniforms.y;" "\n"
    "  float biasFactor = shadowUniforms.z;" "\n"
    "  float shadowSoftScatter = shadowUniforms.w;" "\n"
    "  float visibility = 1.0;" "\n"
    "  vec3 VP = lights[0].position.xyz - v_eyePosition.xyz;" "\n"
    "  VP = normalize(VP);" "\n"
    "  float nDotVP = max(0.0, dot(v_transformedNormal, VP));" "\n"
    "  float bias = biasFactor * tan(acos(nDotVP));" "\n"
    "  bias = clamp(bias, 0.0, 0.01);" "\n"
    "  vec3 shadow_coord = (biasedMvp * vec4(v_worldPosition, 1.0)).xyz;" "\n"
    "  shadow_coord.y = 1 - shadow_coord.y;" "\n"
    "  if (hardShadows > 0.5) {" "\n"
    "    visibility -= shadowSub * (1 - texture(shadowMap, vec3(shadow_coord.xy, shadow_coord.z - bias)));" "\n"
    "  } else {" "\n"
    "     const int totalPasses = 16;" "\n"
    "     float passSub = shadowSub / float(totalPasses);" "\n"
    "     const bool bailEarly = true;" "\n"
    "     int numPassesEstimate = 4;" "\n"
    "     if (!bailEarly) numPassesEstimate = totalPasses;" "\n"
    "     for (int i = 0; i < numPassesEstimate; i++) {" "\n"
    "       // int index = i;" "\n"
    "       // int index = int(float(totalPasses) * random(vec4(floor(v_position.xyz * 1000.0), i))) % 16;" "\n"
    "       int index = int(float(totalPasses) * random(vec4(gl_FragCoord.xyy, i))) % totalPasses;" "\n"
    "       visibility -= passSub * (1.0 - texture(shadowMap, vec3(shadow_coord.xy + poissonDisk16[index] / shadowSoftScatter, shadow_coord.z - bias)));" "\n"
    "     }" "\n"
    "     if (bailEarly && visibility < 1.0) {" "\n"
    "       if (visibility > 1.0 - (passSub * 4.0)) {" "\n"
    "         for (int i = numPassesEstimate; i < totalPasses; i++) {" "\n"
    "           // int index = i;" "\n"
    "           // int index = int(float(totalPasses) * random(vec4(floor(v_position.xyz * 1000.0), i))) % totalPasses;" "\n"
    "           int index = int(float(totalPasses) * random(vec4(gl_FragCoord.xyy, i))) % totalPasses;" "\n"
    "           visibility -= passSub * (1.0 - texture(shadowMap, vec3(shadow_coord.xy + poissonDisk16[index] / shadowSoftScatter, shadow_coord.z - bias)));" "\n"
    "         }" "\n"
    "       } else {" "\n"
    "         visibility = 1.0 - shadowSub;" "\n"
    "       }" "\n"
    "     }" "\n"
    "  }" "\n"
    "  return localColor * visibility;" "\n"
    "}" "\n"
  ;

  float ambient[4];
  redFMaterialGetAmbientColor(material, ambient);
  float diffuse[4];
  redFMaterialGetDiffuseColor(material, diffuse);
  float emissive[4];
  redFMaterialGetEmissiveColor(material, emissive);
  float specular[4];
  redFMaterialGetSpecularColor(material, specular);
  float shininess = redFMaterialGetShininess(material);

  RedFMaterialSettings settings;
  settings.ambient[0]     = ambient[0];
  settings.ambient[1]     = ambient[1];
  settings.ambient[2]     = ambient[2];
  settings.ambient[3]     = ambient[3];
  settings.diffuse[0]     = diffuse[0];
  settings.diffuse[1]     = diffuse[1];
  settings.diffuse[2]     = diffuse[2];
  settings.diffuse[3]     = diffuse[3];
  settings.emissive[0]    = emissive[0];
  settings.emissive[1]    = emissive[1];
  settings.emissive[2]    = emissive[2];
  settings.emissive[3]    = emissive[3];
  settings.specular[0]    = specular[0];
  settings.specular[1]    = specular[1];
  settings.specular[2]    = specular[2];
  settings.specular[3]    = specular[3];
  settings.shininess      = shininess;
  settings.customUniforms = customUniforms;
  settings.postFragment   = postFragment;
  redFMaterialSetup(material, &settings);
}

void shadowMapBegin(RedFHandleLight light, float fustrumSize, float nearClip, float farClip) {
  float left   =-fustrumSize / 2.0;
  float right  = fustrumSize / 2.0;
  float top    = fustrumSize / 2.0;
  float bottom =-fustrumSize / 2.0;
  auto ortho = glm::ortho(left, right, bottom, top, nearClip, farClip);
  glm::mat4 lightMat4;
  redFNodeGetGlobalTransformMatrix(redFLightCastToNode(light), glm::value_ptr(lightMat4));
  auto view = glm::inverse(lightMat4);
  auto viewProjection = ortho * view;
  auto bias = glm::mat4(
    0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0);
  lastBiasedMatrix = bias * viewProjection;
  redFShaderBegin(writeMapShader[0]);
  redFFboBegin(fbo[0], 0);
  redFPushView();
  redFSetMatrixMode(REDF_MATRIX_MODE_PROJECTION);
  redFLoadMatrix(glm::value_ptr(ortho));
  redFSetMatrixMode(REDF_MATRIX_MODE_MODEL_VIEW);
  redFLoadViewMatrix(glm::value_ptr(view));
  redFViewport(0, 0, fboSize, fboSize, redFIsVFlipped());
  redFClear(0, 0, 0, 0);
  redFEnableCulling(0x0404); // GL_FRONT
}

void shadowMapEnd() {
  redFDisableCulling();
  redFShaderEnd(writeMapShader[0]);
  redFPopView();
  redFFboEnd(fbo[0]);
}

void shadowMapUpdateMaterial(RedFHandleMaterial material) {
  redFMaterialBegin(material);
  redFMaterialSetCustomUniformMatrix4f(material, "biasedMvp", glm::value_ptr(lastBiasedMatrix));
  redFMaterialSetCustomUniform4f(material, "shadowUniforms", shadowSub, hardShadows ? 1.f : 0.f, biasFactor, shadowSoftScatter);
  redFMaterialSetCustomUniformFboDepth(material, "shadowMap", fbo[0], 1);
  redFMaterialEnd(material);
}

RedFHandleAssimp *            model         = 0;
RedFHandleMesh *              modelMesh     = 0;
RedFHandleFirstPersonCamera * camera        = 0;
RedFHandleLight *             light         = 0;
float                         fustrumSize   = 100; // Min: 10, max: 1000
float                         farClip       = 300; // Min: 10, max: 1000
RedFBool32                    enableShadows = 1;
RedFHandleMesh *              ground        = 0;
RedFHandleMaterial *          material      = 0;
RedFHandleManipulator *       manipulator   = 0;

void setup() {
  model = redFCreateAssimp(1);
  redFAssimpLoadModel(model[0], "amua.obj", 0);
  modelMesh = redFCreateMesh(redFAssimpGetNumMeshes(model[0]));
  for (unsigned i = 0; i < redFAssimpGetNumMeshes(model[0]); i += 1) {
    redFAssimpGetMesh(model[0], i, modelMesh[i]);
  }
  shadowMapSetup(4096, SHADOW_MAP_RESOLUTION_32);

  material = redFCreateMaterial(1);
  RedFMaterialSettings settings;
  settings.diffuse[0]     = 0.8f;
  settings.diffuse[1]     = 0.8f;
  settings.diffuse[2]     = 0.8f;
  settings.diffuse[3]     = 1.0f;
  settings.ambient[0]     = 0.2f;
  settings.ambient[1]     = 0.2f;
  settings.ambient[2]     = 0.2f;
  settings.ambient[3]     = 1.0f;
  settings.specular[0]    = 0.0f;
  settings.specular[1]    = 0.0f;
  settings.specular[2]    = 0.0f;
  settings.specular[3]    = 1.0f;
  settings.emissive[0]    = 0.0f;
  settings.emissive[1]    = 0.0f;
  settings.emissive[2]    = 0.0f;
  settings.emissive[3]    = 1.0f;
  settings.shininess      = 0.2f;
  settings.postFragment   = 0;
  settings.customUniforms = 0;
  redFMaterialSetup(material[0], &settings);
  shadowMapSetupMaterialWithShadowMap(material[0]);

  light = redFCreateLight(1);
  redFLightEnable(light[0]);
  camera = redFCreateFirstPersonCamera(1);
  redFNodeSetGlobalPosition(redFFirstPersonCameraCastToNode(camera[0]), 50, 80, 120);
  redFNodeLookAt(redFFirstPersonCameraCastToNode(camera[0]), 0, 20, 0, 0, 1, 0);
  redFFirstPersonCameraControlEnable(camera[0]);
  ground = redFCreateMesh(1);
  redFGetPlanePrimitiveMesh(400, 400, 2, 2, ground[0]);

  manipulator = redFCreateManipulator(1);
  redFManipulatorToggleTranslation(manipulator[0]);
}

void update() {
  glfwPollEvents();
}

void draw() {
  redFEnableDepthTest();

  float mat4[16];
  redFManipulatorGetMatrix(manipulator[0], mat4);
  float vec3[3];
  redFManipulatorGetTranslation(manipulator[0], vec3);

  redFNodeSetGlobalPosition(redFLightCastToNode(light[0]), -150, 200, 150);
  redFNodeLookAt(redFLightCastToNode(light[0]), vec3[0], vec3[1], vec3[2], 0, 1, 0);

  if (enableShadows == 1) {
    shadowMapBegin(light[0], fustrumSize, 1, farClip);
    redFPushMatrix();
    redFMultMatrix(mat4);
    for (unsigned i = 0; i < redFAssimpGetNumMeshes(model[0]); i += 1) {
      redFMeshDraw(modelMesh[i]);
    }
    redFPopMatrix();
    shadowMapEnd();

    shadowMapUpdateMaterial(material[0]);
  }

  redFFirstPersonCameraBegin(camera[0]);
  redFMaterialBegin(material[0]);
  redFPushMatrix();
  redFRotateDeg(-90, 1, 0, 0);
  redFMeshDraw(ground[0]);
  redFPopMatrix();
  redFPushMatrix();
  redFMultMatrix(mat4);
  for (unsigned i = 0; i < redFAssimpGetNumMeshes(model[0]); i += 1) {
    redFMeshDraw(modelMesh[i]);
  }
  redFPopMatrix();
  redFMaterialEnd(material[0]);
  redFNodeDraw(redFLightCastToNode(light[0]));
  redFManipulatorDraw(manipulator[0], redFFirstPersonCameraCastToCamera(camera[0]));
  redFFirstPersonCameraEnd(camera[0]);

  redFDisableDepthTest();
  redFFboDepthDraw(fbo[0], redFGetWidth() - 256, 0, 256, 256);
}

int main() {
  RedFEvents events = {};
  events.setup  = setup;
  events.update = update;
  events.draw   = draw;
  return redFMain(&events, 1920, 1080, REDF_WINDOW_MODE_WINDOW, 1, 1, 1, 1, 0, 0);
}
