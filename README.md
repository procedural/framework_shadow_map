![](screenshot.png)

How to run `shadow_map` program on Ubuntu 16.04+:

```
git clone https://github.com/redgpu/framework
git clone https://github.com/procedural/framework_shadow_map
LD_LIBRARY_PATH=framework ./framework_shadow_map/shadow_map
```

How to run `shadow_map.exe` program on Windows 10+:

```
git clone https://github.com/redgpu/framework
git clone https://github.com/procedural/framework_shadow_map
```
Copy all the `.dll` files from `framework` folder to `framework_shadow_map` folder, run the `framework_shadow_map/shadow_map.exe` program.
